﻿using UnityEngine;
using System.Collections;

public class Billboard : MonoBehaviourExt {
	
	public bool lookAt = false;
	public Transform cam;
	
	void Start() {
		
		if (this.cam == null) this.cam = Camera.main.transform;
		
	}
	
	void LateUpdate() {
		
		if (this.cam != null) {
			
			if (this.lookAt == true) {
				
				Vector3 startRot = this.transform.eulerAngles;
				this.transform.LookAt(this.cam);
				this.transform.eulerAngles = new Vector3(startRot.x, this.transform.eulerAngles.y, startRot.z);
				
			} else {
				
				this.transform.rotation = this.cam.rotation;
				
			}
			
		}
		
	}
}
