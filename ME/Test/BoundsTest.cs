﻿using UnityEngine;
using System.Collections;
using ME;

public class BoundsTest : MonoBehaviour {
	
	public MECollider col1;
	public MECollider col2;
	
	public void OnDrawGizmos() {
		
		ME.MEMath.Bounds.HitInfo hitInfo;
		
		if (this.col1.Intersects(this.col2, out hitInfo) == true) {
			
			Gizmos.DrawSphere(hitInfo.collisionPoint, 1f);
			
		}
		
	}
	
}
