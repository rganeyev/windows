﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class StaticBatching {
	
	public static void Batch(GameObject objectsRoot) {
		
		StaticBatching.Batch(objectsRoot, objectsRoot.transform);
		
	}
	
	public static void Batch(GameObject objectsRoot, Transform storageObjectsRoot) {
		
		var item = new StaticBatchingItem();
		item.minVertexCount = 0;
		item.withGrid = false;
		item.Batch(objectsRoot, storageObjectsRoot);
		
	}
	
	public static void BatchAndSave(GameObject root) {
#if UNITY_EDITOR	
		var item = new StaticBatchingItem();
		item.minVertexCount = 0;
		item.withGrid = false;
		item.root = root;
		List<Mesh> meshes = item.Batch();
		//Debug.LogWarning("objs " + objs.Count);

		int i = 1;
		foreach (Mesh mesh in meshes)
		{
			//string name = UnityEditor.AssetDatabase.GetAssetPath(root);
			UnityEditor.AssetDatabase.CreateAsset(mesh, "Assets/" + root.name + "_" + i + ".asset" );
			i++;
		}
		UnityEditor.AssetDatabase.SaveAssets();
#endif
	}

}

[System.Serializable]
public class StaticBatchingItem {
	
	public class BatchItem {
		
		public class CombineItem {
			
			public List<List<CombineInstance>> items = new List<List<CombineInstance>>();
			public int vertexCount;
			
			public CombineItem(Transform root, Mesh mesh) {
				
				this.items = new List<List<CombineInstance>>() { new List<CombineInstance>() };
				this.vertexCount = 0;
				
				this.Add(root, mesh);
				
			}
			
			public void Add(Transform root, Mesh mesh) {
				
				var ci = new CombineInstance();
				ci.mesh = mesh;
				ci.transform = root.localToWorldMatrix;
				
				int newCount = mesh.vertexCount + this.vertexCount;
				if (newCount >= ushort.MaxValue) {
					
					// Create new list
					this.items.Add(new List<CombineInstance>() { ci });
					newCount = mesh.vertexCount;
					
				} else {
					
					this.items[this.items.Count - 1].Add(ci);
					
				}
				
				this.vertexCount = newCount;
				
			}
			
			public List<Mesh> Batch(Transform root, Material key, string itemKey) {
				
				int i = 0;
				var meshes = new List<Mesh>();
				foreach (var item in this.items) {
					
					MeshFilter mf = null;
					if (Application.isPlaying == true) {
						
						GameObject _root = new GameObject("BatchingItem_" + itemKey + "_" + i);
						//_root.SetLayer(root.gameObject.layer);
						_root.transform.parent = root;
						_root.transform.localScale = Vector3.one;
						//_root.isStatic = true;
						
						mf = _root.AddComponent<MeshFilter>();
						var mr = _root.AddComponent<MeshRenderer>();
						mr.sharedMaterial = key;
						
					}
					
					var mesh = new Mesh();
					mesh.CombineMeshes(item.ToArray());
					
					//mesh.Optimize();
					mesh.RecalculateBounds();
					mesh.RecalculateNormals();
					//mesh.UploadMeshData(true);
					
					if (Application.isPlaying == true) {
						
						mf.sharedMesh = mesh;
						
					}
					
					meshes.Add(mesh);
					
					/*
					var rbb = _root.AddComponent<RenderByBounds>();
					rbb.Init();
					*/
					++i;
					
				}
				
				this.items.Clear();
				this.items = null;
				
				return meshes;
				
			}
			
		}
		
		private Transform baseRoot;
		public GameObject root;
		public Dictionary<string, CombineItem> items = new Dictionary<string, CombineItem>();
		
		public BatchItem(string key, Transform baseRoot, Vector2 coords, Transform meshRoot, Mesh mesh) {
			
			this.baseRoot = baseRoot;
			
			if (Application.isPlaying == true) {
				
				this.root = new GameObject("StaticBatchingBy_" + key);
				this.root.transform.parent = baseRoot;
				this.root.transform.localRotation = Quaternion.identity;
				this.root.transform.localPosition = Vector3.zero;
				this.root.transform.localScale = Vector3.one;
				
			}
			
			this.items = new Dictionary<string, CombineItem>();
			
			this.Add(coords, meshRoot, mesh);
			
		}
		
		public void Add(Vector2 coords, Transform meshRoot, Mesh mesh) {
			
			int x = (int)coords.x;
			int y = (int)coords.y;
			
			var key = "x" + x + "_y" + y;
			
			if (this.items.ContainsKey(key) == true) {
				
				this.items[key].Add(meshRoot, mesh);
				
			} else {
				
				this.items.Add(key, new CombineItem(meshRoot, mesh));
				
			}
			
		}
		
		public List<Mesh> Batch(Material key) {
			
			var meshes = new List<Mesh>();
			foreach (var itemList in this.items) {
				
				meshes.AddRange(itemList.Value.Batch(this.root ? this.root.transform : null, key, itemList.Key));
				
			}
			
			if (Application.isPlaying == true) {
				
				this.root.SetLayers(this.baseRoot.gameObject.layer);
				//this.root.isStatic = true;
				
			}
			
			this.items.Clear();
			this.items = null;
			
			return meshes;
			
		}
		
	}
	
	public int minVertexCount = 180;
	
	public GameObject root;
	public Transform storeRoot;
	private Dictionary<Material, BatchItem> batchItems = new Dictionary<Material, BatchItem>();
	
	#region GRID
	public bool withGrid = true;
	public bool showGizmos = false;
	public Vector3 offset;
	public int sizeX = 200;
	public int sizeY = 200;
	public int stepX = 40;
	public int stepY = 40;
	public int height = 100;
	#endregion
	
	public void Batch(GameObject batchObjectsRoot, Transform storeRoot) {
		
		this.root = batchObjectsRoot;
		
		this.Batch(storeRoot);
		
	}
	
	public void Batch(Transform storeRoot) {
		
		this.storeRoot = storeRoot;
		
		this.Batch();
		
	}
	
	public List<Mesh> Batch() {
		
		MeshRenderer[] mrs = this.root.GetComponentsInChildren<MeshRenderer>(true);
		
		// Grouping by materials
		// Exclude objects with materials count 2+
		
		foreach (MeshRenderer mr in mrs) {
			
			if (mr.sharedMaterials.Length == 1) {
				
				var key = mr.sharedMaterial;
				var pos = mr.bounds.center;
				var coords = this.withGrid ? new Vector2(Mathf.RoundToInt(pos.x / this.stepX) * this.stepX, Mathf.RoundToInt(pos.z / this.stepY) * this.stepY) : Vector2.zero;
				
				var mf = mr.GetComponent<MeshFilter>();
				if (mf != null) {
					
					var ignore = StaticBatchingItem.FindInParents<StaticBatchingIgnore>(mf.gameObject);
					if (ignore != null) continue;
					
					if (mf.sharedMesh.vertexCount > this.minVertexCount) {
						
						if (this.batchItems.ContainsKey(key) == true) {
							
							this.batchItems[key].Add(coords, mr.transform, mf.sharedMesh);
							
						} else {
							
							this.batchItems.Add(key, new BatchItem(key.name, this.storeRoot, coords, mr.transform, mf.sharedMesh));
							
						}
						
						if (Application.isPlaying == true) {
							
							Component.Destroy(mr);
							Component.Destroy(mf);
							
						}
						
					}
					
				}
				
			}
			
		}
		
		var meshes = new List<Mesh>();
		foreach (var item in this.batchItems) {
			
			var key = item.Key;
			var batchItem = item.Value;
			
			meshes.AddRange(batchItem.Batch(key));
			
		}
		
		this.batchItems.Clear();
		this.batchItems = null;
		
		return meshes;
		
	}
	
	static public T FindInParents<T> (GameObject go) where T : Component
	{
		if (go == null) return null;
		object comp = go.GetComponent<T>();

		if (comp == null)
		{
			Transform t = go.transform.parent;

			while (t != null && comp == null)
			{
				comp = t.gameObject.GetComponent<T>();
				t = t.parent;
			}
		}
		return (T)comp;
	}

#if UNITY_EDITOR
	public void OnDrawGizmos(Vector3 startPoint, Quaternion rotation) {
		
		if (this.showGizmos == false) return;
		
		Color c = Color.green;
		c.a = 0.1f;
		Gizmos.color = c;
		
		Matrix4x4 oldMatrix = Gizmos.matrix;
		Matrix4x4 rotationMatrix = Matrix4x4.TRS(startPoint, rotation, Vector3.one);
		
		Gizmos.matrix = rotationMatrix;
		
		for (int i = 0; i < this.sizeX; i += this.stepX) {
			
			for (int j = 0; j < this.sizeY; j += this.stepY) {
				
				Vector3 _offset = new Vector3(i, 0f, j) + this.offset;
				Gizmos.DrawCube(_offset, new Vector3(this.stepX, this.height, this.stepY));
				
			}
			
		}
		
		Gizmos.matrix = oldMatrix;
		
	}
#endif
	
}
