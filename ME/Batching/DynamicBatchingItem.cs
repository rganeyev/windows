﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public static class DynamicBatching {
	
	public static void Batch(GameObject root, bool markAsStaticAfterBatch = false) {
		
		var item = new DynamicBatchingItem();
		item.root = root;
		item.maxVertices = 180;
		item.Batch(markAsStaticAfterBatch);
		
	}


	public static void BatchAndSave(GameObject root) {
		#if UNITY_EDITOR	
		var item = new DynamicBatchingItem();
		item.root = root;
		item.maxVertices = 180;
		List<GameObject> objs = item.Batch(false);
		//Debug.LogWarning("objs " + objs.Count);

		int i = 1;
		foreach (GameObject obj in objs)
		{
			string name = UnityEditor.AssetDatabase.GetAssetPath(root);
			Debug.Log("name " + name);

			UnityEditor.AssetDatabase.CreateAsset( obj.GetComponent<MeshFilter>().sharedMesh, "Assets/" + root.name + "_" + i + ".asset" );
			i++;
		}
		UnityEditor.AssetDatabase.SaveAssets();
	#endif
	}

	
}

[System.Serializable]
public class DynamicBatchingItem {
	
	public GameObject root;
	public int maxVertices = 180;

	public List<GameObject> Batch(bool markAsStaticAfterBatch = false) {
		
		if (this.root == null) return null;

		var filters = this.root.GetComponentsInChildren<MeshFilter>(true);

		List<GameObject> objs = new List<GameObject>();
		foreach (var filter in filters) {
			
			Mesh mesh = filter.sharedMesh;
			if (mesh == null) continue;
			
			var mr = filter.GetComponent<MeshRenderer>();
			if (mesh.vertexCount > this.maxVertices || filter.transform.localScale != Vector3.one) {
				
				if (mr.sharedMaterials.Length == 1) {

					objs.AddRange(this.Split(filter.gameObject, mesh, mr.sharedMaterial, markAsStaticAfterBatch));

					Component.DestroyImmediate(filter);
					Component.DestroyImmediate(mr);
				}
			}
		}

		return objs;
	}
	
	enum AttributeMask {
		Positions = 1,
		Colors = 2,
		UVs = 4,
		UV2s = 8,
		Normals = 16,
		Tangents = 32,
		BoneWeights = 64,
		Bindposes = 128
	};
	
	private List<GameObject> Split(GameObject root, Mesh mesh, Material material, bool markAsStaticAfterBatch) {
		
		int attributeMask = 0;

		attributeMask |= ((mesh.vertices.Length > 0) ? (int)AttributeMask.Positions : 0);
		attributeMask |= ((mesh.colors32.Length > 0) ? (int)AttributeMask.Colors : 0);
		attributeMask |= ((mesh.uv.Length > 0) ? (int)AttributeMask.UVs : 0);
		attributeMask |= ((mesh.uv2.Length > 0) ? (int)AttributeMask.UV2s : 0);
		attributeMask |= ((mesh.normals.Length > 0) ? (int)AttributeMask.Normals : 0);
		attributeMask |= ((mesh.tangents.Length > 0) ? (int)AttributeMask.Tangents : 0);
		attributeMask |= ((mesh.boneWeights.Length > 0) ? (int)AttributeMask.BoneWeights : 0);
		attributeMask |= ((mesh.bindposes.Length > 0) ? (int)AttributeMask.Bindposes : 0);
		
		bool complete = false;
		int offset = 0;
		
		var rootScale = root.transform.localScale;
		root.transform.localScale = Vector3.one;

		List<GameObject> outObjs = new List<GameObject>();
		
		for (int i = 0; !complete; ++i) {
			
			var go = new GameObject("DynamicBatching" + i);
			//go.SetLayer(root.layer);
			go.transform.parent = root.transform;
			go.transform.localPosition = Vector3.zero;
			go.transform.localRotation = Quaternion.identity;
			go.transform.localScale = Vector3.one;
			if (markAsStaticAfterBatch == true) go.isStatic = true;
			
			var mf = go.AddComponent<MeshFilter>();
			var mr = go.AddComponent<MeshRenderer>();
			
			var meshPart = new Mesh();
			
			var indexMap = new Dictionary<int, int>();
			var indexBuffer = new List<int>();
			int vCount = 0;
			
			while (!complete) {

				if (vCount > this.maxVertices - 3) break;
				
				for (int j = 0; j < 3; ++j) {
					
					var startIndex = mesh.triangles[offset + j];
					
					int newIndex;
					if (!indexMap.TryGetValue(startIndex, out newIndex)) {

						newIndex = vCount++;
						indexMap.Add(startIndex, newIndex);

					}

					indexBuffer.Add(newIndex);

				}
				
				offset += 3;
				
				if (offset == mesh.triangles.Length) complete = true;
				
			}
			
			var vertices = ((attributeMask & (int)AttributeMask.Positions) == 0) ? null : new Vector3[vCount];
			var colors32 = ((attributeMask & (int)AttributeMask.Colors) == 0) ? null : new Color32[vCount];
			var uv = ((attributeMask & (int)AttributeMask.UVs) == 0) ? null : new Vector2[vCount];
			var uv2 = ((attributeMask & (int)AttributeMask.UV2s) == 0) ? null : new Vector2[vCount];
			var normals = ((attributeMask & (int)AttributeMask.Normals) == 0) ? null : new Vector3[vCount];
			var tangents = ((attributeMask & (int)AttributeMask.Tangents) == 0) ? null : new Vector4[vCount];
			var boneWeights = ((attributeMask & (int)AttributeMask.BoneWeights) == 0) ? null : new BoneWeight[vCount];
			var bindposes = ((attributeMask & (int)AttributeMask.Bindposes) == 0) ? null : new Matrix4x4[vCount];

			foreach (var indexPair in indexMap) {
				
				if ((attributeMask & (int)AttributeMask.Positions) != 0) {
					
					var v = mesh.vertices[indexPair.Key];
					vertices[indexPair.Value] = new Vector3(v.x * rootScale.x, v.y * rootScale.y, v.z * rootScale.z);
					
				}
				
				if ((attributeMask & (int)AttributeMask.Colors) != 0) colors32[indexPair.Value] = mesh.colors32[indexPair.Key];
				if ((attributeMask & (int)AttributeMask.UVs) != 0) uv[indexPair.Value] = mesh.uv[indexPair.Key];
				if ((attributeMask & (int)AttributeMask.UV2s) != 0) uv2[indexPair.Value] = mesh.uv2[indexPair.Key];
				if ((attributeMask & (int)AttributeMask.Normals) != 0) normals[indexPair.Value] = mesh.normals[indexPair.Key];
				if ((attributeMask & (int)AttributeMask.Tangents) != 0) tangents[indexPair.Value] = mesh.tangents[indexPair.Key];
				if ((attributeMask & (int)AttributeMask.BoneWeights) != 0) boneWeights[indexPair.Value] = mesh.boneWeights[indexPair.Key];
				if ((attributeMask & (int)AttributeMask.Bindposes) != 0) bindposes[indexPair.Value] = mesh.bindposes[indexPair.Key];
				
			}

			if ((attributeMask & (int)AttributeMask.Positions) != 0) meshPart.vertices = vertices;
			if ((attributeMask & (int)AttributeMask.Colors) != 0) meshPart.colors32 = colors32;
			if ((attributeMask & (int)AttributeMask.UVs) != 0) meshPart.uv = uv;
			if ((attributeMask & (int)AttributeMask.UV2s) != 0) meshPart.uv2 = uv2;
			if ((attributeMask & (int)AttributeMask.Normals) != 0) meshPart.normals = normals;
			if ((attributeMask & (int)AttributeMask.Tangents) != 0) meshPart.tangents = tangents;
			if ((attributeMask & (int)AttributeMask.BoneWeights) != 0) meshPart.boneWeights = boneWeights;
			if ((attributeMask & (int)AttributeMask.Bindposes) != 0) meshPart.bindposes = bindposes;

			meshPart.triangles = indexBuffer.ToArray();
			
			//meshPart.Optimize();
			meshPart.RecalculateBounds();
			//meshPart.RecalculateNormals();
			meshPart.UploadMeshData(true);
			
			mf.sharedMesh = meshPart;
			mr.sharedMaterial = material;


			outObjs.Add(go);
			
		}

		return  outObjs;

	}
	
}
