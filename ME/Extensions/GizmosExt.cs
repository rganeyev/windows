﻿using UnityEngine;
using System.Collections;

public static class GizmosExt {
	
	public static void DrawCube(Vector3 position, Vector3 size, Quaternion rotation, bool wired = false) {
		
		Matrix4x4 oldMatrix = Gizmos.matrix;
		Matrix4x4 rotationMatrix = Matrix4x4.TRS(position, rotation, Vector3.one);
		
		Gizmos.matrix = rotationMatrix; 

		if (!wired)
			Gizmos.DrawCube(Vector3.zero, size);
		else
			Gizmos.DrawWireCube(Vector3.zero, size);

		Gizmos.matrix = oldMatrix;
		
	}

}
