public static class CurrentBundle {

	public static readonly string id = "com.creativemobile.roadsmash";
	public static readonly string version = "1.5.00";
	public static readonly int versionInt = 1500;
	public static readonly int versionStrong = 15;
	public static readonly int versionSub = 0;
}

