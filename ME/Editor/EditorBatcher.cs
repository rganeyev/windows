﻿using UnityEngine;
using System.Collections;

public class EditorBatcher : MonoBehaviour {

	public GameObject subject;
	
	[ ContextMenu ("batch mesh")]
	public void Batch()
	{
		DynamicBatching.BatchAndSave(subject);
		//AssetDatabase.CreateAsset( [mesh object here], [path to asset] );
		//AssetDatabase.SaveAssets();
	}
	
#if UNITY_EDITOR
	[UnityEditor.MenuItem("Mesh/Batch selected meshes")]
	static void CreateStaticBatching() {
		
		var obj = UnityEditor.Selection.activeGameObject;
		/*
		var filters = obj.GetComponentsInChildren<MeshFilter>(true);
		
		var list = new Mesh();
		foreach (var filter in filters) {
			
			if (filter.sharedMesh != null) {
				
				list.Add(filter.sharedMesh);
				
			}
			
		}*/
		
		StaticBatching.BatchAndSave(obj);
		
	}
#endif
	
}
