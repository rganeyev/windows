﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LBVfxSpawnPool : MonoBehaviour {

	public static LBVfxSpawnPool instance;

	[System.Serializable]
	public class Item {
		public ParticleSystem prefab;
		public int maxCount;
	}
	
	public Item[] items;
	private List<ParticleSystem>[] pools;

	void Start()
	{
		instance = this;
		int poolCount = 0;
		foreach (var item in this.items) 
		{
			poolCount += item.maxCount;
		}

		pools = new List<ParticleSystem>[items.Length];
		for (int k = 0; k < pools.Length; k++) 
		{
			pools[k] = new List<ParticleSystem>(items[k].maxCount);
		}
		FillPool();
	}

	protected virtual void OnDestroy()
	{
		instance = null;
	}

	void FillPool() 
	{
		for (int k = 0; k < pools.Length; k++) 
		{
			var item = items[k];
			for (int i = 0; i < item.maxCount; ++i) 
			{
				ParticleSystem obj = Instantiate(item.prefab, Vector3.zero, Quaternion.identity) as ParticleSystem;

				obj.transform.parent = this.transform;
				obj.gameObject.SetActive(false);
				pools[k].Add(obj);
			}
		}
	}
	
	public ParticleSystem Spawn(int poolIndex, Vector3 position, Quaternion rotation) 
	{
		foreach (var item in pools[poolIndex]) 
		{
			if (item.gameObject.activeSelf == false) 
			{
				item.transform.position = position;
				item.transform.rotation = rotation;
				item.gameObject.SetActive(true);
				item.Stop();
				item.Play();
				return item;
			}
		}
		
		return null;
	}
	
	public void Despawn(ParticleSystem item) {

		item.transform.parent = this.transform;
		item.gameObject.SetActive(false);
	}
	
}
