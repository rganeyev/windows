﻿using System.Linq;
using UnityEngine;

public static class MathX
{
    #region BOUNDS

    public struct Bounds
    {
        public Vector3 center;
        public Vector3 size;

// ReSharper disable InconsistentNaming
        private Quaternion _rotation;

        private Vector3 _eulerAngles;
        private Vector3 _extents;

        public Quaternion rotation
        {
            get { return _rotation; }
            private set { _rotation = value; }
        }

        public Vector3 eulerAngles
        {
            get { return _eulerAngles; }
            private set { _eulerAngles = value; }
        }

        public Vector3 extents
        {
            get { return _extents; }
            private set { _extents = value; }
        }

// ReSharper restore InconsistentNaming

        public Bounds(Vector3 center, Vector3 size, Quaternion rotation)
        {
            this.center = center;
            this.size = size;
            _rotation = rotation;

            _eulerAngles = rotation.eulerAngles;
            _extents = this.size/2f;
        }

        public Bounds(Vector3 center, Vector3 size, Vector3 eulerAngles)
        {
            this.center = center;
            this.size = size;
            _rotation = Quaternion.Euler(eulerAngles);

            _eulerAngles = eulerAngles;
            _extents = this.size/2f;
        }

        public void Rotate(Quaternion newRotation)
        {
            rotation *= newRotation;
            eulerAngles = rotation.eulerAngles;
        }

        public void Rotate(Vector3 eulerAngles)
        {
            this.eulerAngles += eulerAngles;
            rotation = Quaternion.Euler(this.eulerAngles);
        }

        public bool Contains(Vector3 point)
        {
            Quaternion rotation = Quaternion.Inverse(this.rotation);
            Vector3 checkPoint = rotation*(point - center);

            return checkPoint.x <= extents.x && checkPoint.y <= extents.y && checkPoint.z <= extents.z &&
                   checkPoint.x >= -extents.x && checkPoint.y >= -extents.y && checkPoint.z >= -extents.z;
        }

        public bool Contains(Bounds bounds)
        {
            var points = GetPoints();
            if (points.Any(bounds.Contains))
            {
                return true;
            }

            points = bounds.GetPoints();
            return points.Any(Contains);
        }

        public bool Contains(Bounds bounds, Bounds prevBounds)
        {
            /*
			if (prevBounds.center == Vector3.zero) {
				
				return this.Contains(bounds);
				
			}
			
			var d = Vector3.Distance(bounds.center, prevBounds.center);
			
			bool contains = false;
			if (d > bounds.size.z * 2f && d < bounds.size.z * 10f) {
				
				//Debug.LogError("MISS: " + bounds.center + " :: PREV: " + prevBounds.center);
				
				var num = Mathf.CeilToInt(d / bounds.size.z);
				
				Vector3 v;
				Quaternion q;
				float k = 0f;
				for (int i = 0; i < num; ++i) {
					
					k = i / (float)num;
					v = Vector3.Lerp(prevBounds.center, bounds.center, k);
					q = Quaternion.Slerp(prevBounds.rotation, bounds.rotation, k);
					
					//Debug.LogError("CHECK MISS: " + v + " :: " + (i / (float)num));
					
					contains = this.Contains(new MathX.Bounds(v, bounds.size, q));
					if (contains == true) break;
					
				}
				
			} else {
				
				contains = this.Contains(bounds);
				
			}
			
			return contains;
			*/

            return Contains(bounds);
        }

        public Vector3[] GetPoints()
        {
            const int imax = 12;
            var points = new Vector3[imax];

            points[0] = -extents;
            points[1] = new Vector3(extents.x, -extents.y, extents.z);
            points[2] = new Vector3(-extents.x, -extents.y, extents.z);
            points[3] = new Vector3(extents.x, -extents.y, -extents.z);

            points[4] = extents;
            points[5] = new Vector3(-extents.x, extents.y, -extents.z);
            points[6] = new Vector3(-extents.x, extents.y, extents.z);
            points[7] = new Vector3(extents.x, extents.y, -extents.z);

            points[8] = new Vector3(extents.x, 0f, extents.z);
            points[9] = new Vector3(-extents.x, 0f, -extents.z);
            points[10] = new Vector3(-extents.x, 0f, extents.z);
            points[11] = new Vector3(extents.x, 0f, -extents.z);

            for (var i = 0; i < imax; ++i) points[i] = center + rotation*points[i];

            return points;
        }

        public Vector3 GetForwardPoint(Vector3 side)
        {
            var point = new Vector3(0f, 0f, extents.z) + side;

            return center + rotation*point;
        }

        public Vector3 GetForwardPointRight()
        {
            return GetForwardPoint(new Vector3(extents.x, 0f, 0f));
        }

        public Vector3 GetForwardPointLeft()
        {
            return GetForwardPoint(new Vector3(-extents.x, 0f, 0f));
        }

        public Vector3 GetBackPoint(Vector3 side)
        {
            var point = new Vector3(0f, 0f, -extents.z) - side;

            return center + rotation*point;
        }

        public Vector3 GetBackPointRight()
        {
            return GetBackPoint(new Vector3(-extents.x, 0f, 0f));
        }

        public Vector3 GetBackPointLeft()
        {
            return GetBackPoint(new Vector3(extents.x, 0f, 0f));
        }

        public bool IsBack(Bounds current, Bounds before, bool debug = false)
        {
            Vector3 collisionPoint;
            bool fromRight;
            return IsBack(current, before, out fromRight, out collisionPoint, debug);
        }

        public bool IsBack(Bounds current, Bounds before, out Vector3 collisionPoint, bool debug = false)
        {
            bool fromRight;
            return IsBack(current, before, out fromRight, out collisionPoint, debug);
        }

        public bool IsBack(Bounds current, Bounds before, out bool fromRight, bool debug = false)
        {
            Vector3 collisionPoint;
            return IsBack(current, before, out fromRight, out collisionPoint, debug);
        }

        public bool IsBack(Bounds current, Bounds before, out bool fromRight, out Vector3 collisionPoint,
            bool debug = false)
        {
            // Вычисляем сторону откуда я въехал
            fromRight = true;
            if (before.center.z > current.center.z) fromRight = false;

            var plane = new Plane(new Vector3(1f, 0f, 0f), GetForwardPoint(Vector3.zero));

            if (debug)
            {
                Gizmos.color = Color.green;
                var br = GetForwardPointRight();
                var bl = GetForwardPointLeft();
                var b1 = GetForwardPoint(Vector3.zero);
                Gizmos.DrawCube(b1, Vector3.one);
                Gizmos.DrawCube(br, Vector3.one);
                Gizmos.DrawCube(bl, Vector3.one);
            }

            return CheckBack(fromRight, plane, current, before, out collisionPoint, debug) ||
                   CheckBack(!fromRight, plane, current, before, out collisionPoint, debug);
        }

        private bool CheckBack(bool fromRight, Plane plane, Bounds current, Bounds before, out Vector3 collisionPoint,
            bool debug = false)
        {
            // Если я въехал с правой стороны - проверяем вхождение луча с правой передней точки before до передней точки current
            var point1 = fromRight ? before.GetForwardPointLeft() : before.GetForwardPointRight();
            var point2 = fromRight ? current.GetForwardPointLeft() : current.GetForwardPointRight();
            var ray = new Ray(point1, point2 - point1);
            var d = Vector3.Distance(point1, point2);

            if (debug)
            {
                Gizmos.color = fromRight ? Color.red : Color.blue;
                Gizmos.DrawLine(point1, point2);
            }

            collisionPoint = point1;

            float dist;
            if (!plane.Raycast(ray, out dist)) return false;
            collisionPoint = ray.GetPoint(dist);

            // Если дистанция от точки до точки меньше, чем до плейна - мы не могли въехать в жопу
            if (!(dist <= d)) return false;
            // Если попали в размер this.bounds - въехали в жопу
            if (!debug) return true;
            Gizmos.color = Color.yellow;
            Gizmos.DrawSphere(collisionPoint, 1f);

            //var zOffset = fromRight ? (p.z > this.GetBackPointRight().z) : (p.z < this.GetBackPointLeft().z);
            //if (zOffset == true) {

            // Вошли в жопу
            return true;

            //}

            // Вошли скраю
        }
    }

    #endregion

    #region MATH

    public static float RoundBy2(float value)
    {
        return ((int) (value*100f))/100f;
    }

    public static Vector2 RoundBy2(Vector2 v)
    {
        return new Vector2(RoundBy2(v.x), RoundBy2(v.y));
    }

    public static Vector3 RoundBy2(Vector3 v)
    {
        return new Vector3(RoundBy2(v.x), RoundBy2(v.y), RoundBy2(v.z));
    }

    public static Vector4 RoundBy2(Vector4 v)
    {
        return new Vector4(RoundBy2(v.x), RoundBy2(v.y), RoundBy2(v.z), RoundBy2(v.w));
    }

    public static Quaternion RoundBy2(Quaternion v)
    {
        return new Quaternion(RoundBy2(v.x), RoundBy2(v.y), RoundBy2(v.z), RoundBy2(v.w));
    }

    public static float GetPerc(int current, int max)
    {
        return GetPerc(current, (float) max);
    }

    public static float GetPerc(float current, float max)
    {
        if (max == 0) return 0;
        return current/max;
    }

    public static float GetPercentFrom(float percent, float value)
    {
        return value*percent;
    }

    /// <summary>
    /// percent - from 0f to 1f.
    /// </summary>
    public static int GetPercentFrom(float percent, int value)
    {
        return Mathf.FloorToInt(value*percent);
    }

    /// <summary>
    /// percent - from 0 to 100.
    /// </summary>
    public static int GetPercentFrom(int percent, int value)
    {
        return GetPercentFrom(percent/100f, value);
    }

    #endregion

    #region GEOMETRY

    public static Vector2 ArrangeByCircle(int iteration, int count, float angle, float radius, float startAngle = 0f)
    {
        angle = angle*Mathf.Deg2Rad;
        startAngle = startAngle*Mathf.Deg2Rad;

        Vector2 v = Vector2.zero;
        v.x = Mathf.Sin(angle/count*iteration + startAngle)*radius;
        v.y = Mathf.Cos(angle/count*iteration + startAngle)*radius;

        return v;
    }

    public static Vector2 ByGrid(Vector2 value, Vector2 grid)
    {
        return new Vector2(Mathf.Ceil(value.x/grid.x)*grid.x, Mathf.Ceil(value.y/grid.y)*grid.y);
    }

    public static void GenerateTriangles(int width, int height, out int[] triangles)
    {
        triangles = new int[(width - 1)*(height - 1)*6];

        int index = -1;
        for (int y = 0; y < height - 1; ++y)
        {
            for (int x = 0; x < width - 1; ++x)
            {
                int offset = y*((width)*4) + (x*4);

                triangles[++index] = offset;
                triangles[++index] = offset + 1;
                triangles[++index] = offset + 2;

                triangles[++index] = offset + 1;
                triangles[++index] = offset + 3;
                triangles[++index] = offset + 2;
            }
        }
    }

    #endregion
}