using UnityEngine;

public static class TweenerExt
{
	// position
	public static LaikaBOSS.Tweener.Tween<Transform> addTween(this LaikaBOSS.Tweener tweener, Transform transform, float duration, Vector3 start, Vector3 end)
	{
		return tweener.addTween(transform, duration, 0.0f, 1.0f)
			.onUpdate((_, t) => transform.localPosition = Vector3.Lerp(start, end, t));
	}

	public static LaikaBOSS.Tweener.Tween<Transform> addTween(this LaikaBOSS.Tweener tweener, Transform transform, float duration, Vector3 end)
	{
		return tweener.addTween(transform, duration, transform.localPosition, end);
	}

	// rotation
	public static LaikaBOSS.Tweener.Tween<Transform> addTween(this LaikaBOSS.Tweener tweener, Transform transform, float duration, Quaternion start, Quaternion end)
	{
		return tweener.addTween(transform, duration, 0.0f, 1.0f)
			.onUpdate((_, t) => transform.localRotation = Quaternion.Slerp(start, end, t));
	}

	public static LaikaBOSS.Tweener.Tween<Transform> addTween(this LaikaBOSS.Tweener tweener, Transform transform, float duration, Quaternion end)
	{
		return tweener.addTween(transform, duration, transform.localRotation, end);
	}

	// scale
	public static LaikaBOSS.Tweener.Tween<Transform> addTweenScale(this LaikaBOSS.Tweener tweener, Transform transform, float duration, Vector3 start, Vector3 end)
	{
		return tweener.addTween(transform, duration, 0.0f, 1.0f)
			.onUpdate((_, t) => transform.localScale = Vector3.Lerp(start, end, t));
	}

	public static LaikaBOSS.Tweener.Tween<Transform> addTweenScale(this LaikaBOSS.Tweener tweener, Transform transform, float duration, Vector3 end)
	{
		return tweener.addTweenScale(transform, duration, transform.localScale, end);
	}

	
	// euler rotation
	public static LaikaBOSS.Tweener.Tween<Transform> addTweenEuler(this LaikaBOSS.Tweener tweener, Transform transform, float duration, Vector3 start, Vector3 end)
	{
		return tweener.addTween(transform, duration, 0.0f, 1.0f)
			.onUpdate((_, t) =>
			{
				/*var euler = transform.localEulerAngles;
				euler.x = Mathf.Lerp(start.x, end.x, t);
				euler.y = Mathf.Lerp(start.y, end.y, t);
				euler.z = Mathf.Lerp(start.z, end.z, t);*/
				transform.eulerAngles = Vector3.Lerp(start, end, t);
			});
	}

	public static LaikaBOSS.Tweener.Tween<Transform> addTweenEuler(this LaikaBOSS.Tweener tweener, Transform transform, float duration, Vector3 end)
	{
		return tweener.addTweenEuler(transform, duration, transform.eulerAngles, end);
	}

	
	private static void _setZAngle(Transform transform, float angle)
	{
		var euler = transform.localEulerAngles;
		euler.z = angle;
		transform.localEulerAngles = euler;
	}
	
	public static LaikaBOSS.Tweener.Tween<Transform> addTweenZRotation(this LaikaBOSS.Tweener tweener, Transform transform, float duration, float start, float end)
	{
		return tweener.addTween(transform, duration, start, end)
			.onUpdate(_setZAngle);
	}

	public static LaikaBOSS.Tweener.Tween<Transform> addTweenZRotation(this LaikaBOSS.Tweener tweener, Transform transform, float duration, float end)
	{
		return tweener.addTweenZRotation(transform, duration, transform.localEulerAngles.z, end);
	}
}
