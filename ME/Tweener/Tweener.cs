using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace LaikaBOSS {

	public class Tweener: MonoBehaviour
	{
		public interface ITransition
		{
			float interpolate(float start, float distance, float elapsedTime, float duration);
		}
	
		public interface ITween
		{
			bool isCompleted();
			void RaiseCancel();
			void RaiseComplete();
			object getTag();
			void update(float dt);
		}
	
		public class Tween<T>: ITween
		{
			private static int INFINITE_LOOPS = -1;
	
			private T _obj;
	
			private float _duration;
			private float _start;
			
			private class Target
			{
				public float start;
				public float value;
				public float duration;
				public ITransition transition;
				public bool inverse = false;
			}
			
			private List<Target> _targets = new List<Target>();
			int _currentTarget = 0;
			
			private float _elapsed = 0.0f;
			
			private bool _completed = false;
			private object _tag;
			private int _loops = 1;
	
			private System.Action<T, float> _update = null;
			private System.Action<T> _complete = null;
			private System.Action<T> _cancel = null;
	
			public Tween(T obj, float duration, float start, float end)
			{
				_obj = obj;
				Target target = new Target();
				target.start = start;
				target.value = end;
				target.duration = duration;
				target.transition = Ease.Linear;
				_targets.Add(target);
			}
			
			public void RaiseComplete() {
				
				if (_complete != null)
					_complete(_obj);
				
			}
			
			public void RaiseCancel() {
				
				if (_cancel != null)
					_cancel(_obj);
				
			}
	
			public bool isCompleted() { return _completed; }
	
			public object getTag() { return _tag; }
			
			public void update(float dt)
			{
				_elapsed += dt;
				
				var target = _targets[_currentTarget];
				//Debug.Log(_currentTarget + " " + _elapsed + " " + target.duration);
				
				if (_elapsed >= target.duration)
				{
					if ((_currentTarget + 1) == _targets.Count)
					{
						//Debug.Log("all targets done " + _targets.Count);
						if (_update != null)
							_update(_obj, target.inverse ? target.start : target.value);
						
						RaiseComplete();
	
						if (_loops != INFINITE_LOOPS)
							_completed = (--_loops == 0);
		
						if (!_completed)
						{
							_elapsed = 0.0f;
							_currentTarget = 0;
						}
	
						return;
					}
	
					_elapsed = _elapsed - target.duration;
					target = _targets[++_currentTarget];
				}
	
				if (_update != null)
				{
					float t = target.inverse ? target.duration - _elapsed : _elapsed;
					float v = target.transition.interpolate(target.start, target.value - target.start, t, target.duration);
					_update(_obj, v);
				}
	
				return;
			}
	
			public Tween<T> ease(ITransition value)
			{
				_targets[0].transition = value;
	
				return this;
			}
	
			public Tween<T> onUpdate(System.Action<T, float> func)
			{
				_update += func;
				return this;
			}
			
			public Tween<T> onComplete(System.Action<T> func)
			{
				_complete += func;
				return this;
			}
			
			public Tween<T> onCancel(System.Action<T> func)
			{
				_cancel += func;
				return this;
			}
	
			public Tween<T> tag(object value)
			{
				_tag = value;
				return this;
			}
	
			public Tween<T> repeat()
			{
				_loops = INFINITE_LOOPS;
				return this;
			}
	
			public Tween<T> repeat(int loops)
			{
				_loops = loops;
				return this;
			}
	
			public Tween<T> addTarget(float duration, float end)
			{
				var target = new Target();
				target.start = _targets[_targets.Count - 1].value;
				target.value = end;
				target.duration = duration;
				target.transition = _targets[0].transition;
				_targets.Add(target);
				return this;
			}
	
			public Tween<T> addTarget(float duration, float end, ITransition transition)
			{
				var target = new Target();
				target.start = _targets[_targets.Count - 1].value;
				target.value = end;
				target.duration = duration;
				target.transition = transition;
				_targets.Add(target);
				return this;
			}
	
			public Tween<T> reflect()
			{
				List<Target> reflectedTargets = new List<Target>();
				foreach (var target in _targets)
				{
					var reflected = new Target();
					reflected.start = target.start;
					reflected.value = target.value;
					reflected.transition = target.transition;
					reflected.duration = target.duration;
					reflected.inverse = !target.inverse;
					reflectedTargets.Add(reflected);
				}
	
				_targets = _targets.Concat(reflectedTargets).ToList();
				return this;
			}
		}
		
		public enum TimerType { Fixed, Game }
		public TimerType timerType = TimerType.Game;
		public bool repeatByDefault = false;
	
		float _lastTimeValue = -1.0f;
		
		private List<ITween> _tweens = new List<ITween>();
		
		public Tween<T> addTween<T>(T obj, float duration, float start, float end)
		{
			var tween = new Tween<T>(obj, duration, start, end);
			if (repeatByDefault)
				tween.repeat();
			_tweens.Add(tween);
	
			_lastTimeValue = Time.realtimeSinceStartup;
			return tween;
		}
	
		public void removeTweens(object tweenerTag)
		{
			_clean(tween => tween.getTag() == tweenerTag);
		}
		
		void Update()
		{
			if (timerType == TimerType.Fixed)
				_update(Time.realtimeSinceStartup - _lastTimeValue);
			else if (timerType == TimerType.Game)
				_update(Time.deltaTime);
			
			_lastTimeValue = Time.realtimeSinceStartup;
		}
		
		void OnDestroy() {
			
			_tweens.ForEach((tween) => tween.RaiseCancel());
			
		}
		
		void _update(float dt)
		{
			_tweens.ForEach((tween) => tween.update(dt));
			_clean(tween => tween.isCompleted());
		}
	
		void _clean(System.Func<ITween, bool> predicate) {
	
			_tweens = _tweens.Where((tween) => {
				
				if (predicate(tween)) {
					
					tween.RaiseCancel();
					return false;
					
				}
				
				return true;
				
			}).ToList();
	
		}
	
	}
	
	public class EaseTransition: Tweener.ITransition
	{
		private System.Func<float, float, float, float, float> _func;
	
		public EaseTransition(System.Func<float, float, float, float, float> func)
		{
			_func = func;
		}
		
		public float interpolate(float start, float distance, float elapsedTime, float duration)
		{
			return _func(start, distance, elapsedTime, duration);
		}
	}
	
	public class CurveTransition: Tweener.ITransition
	{
		AnimationCurve _curve;
	
		public CurveTransition(AnimationCurve curve)
		{
			_curve = curve;
		}
		
		public float interpolate(float start, float distance, float elapsedTime, float duration)
		{
			float curveDuration = 0.0f;
			if (_curve.length > 0)
				curveDuration = _curve.keys[_curve.length - 1].time;
			
			float t = _curve.Evaluate(curveDuration * elapsedTime / duration);
	
			return start + t * distance;
		}
	}
	
	public class Ease
	{
		public static EaseTransition Linear = new EaseTransition(_linear);
	
		public static EaseTransition InQuad = new EaseTransition(_inQuad);
		public static EaseTransition OutQuad = new EaseTransition(_outQuad);
		public static EaseTransition InOutQuad = new EaseTransition(_inOutQuad);
	
		public static EaseTransition InCubic = new EaseTransition(_inCubic);
		public static EaseTransition OutCubic = new EaseTransition(_outCubic);
		public static EaseTransition InOutCubic = new EaseTransition(_inOutCubic);
		
		public static EaseTransition InQuart = new EaseTransition(_inQuart);
		public static EaseTransition OutQuart = new EaseTransition(_outQuart);
		public static EaseTransition InOutQuart = new EaseTransition(_inOutQuart);
	
		public static EaseTransition InQuint = new EaseTransition(_inQuint);
		public static EaseTransition OutQuint = new EaseTransition(_outQuint);
		public static EaseTransition InOutQuint = new EaseTransition(_inOutQuint);
	
		public static EaseTransition InSine = new EaseTransition(_inSine);
		public static EaseTransition OutSine = new EaseTransition(_outSine);
		public static EaseTransition InOutSine = new EaseTransition(_inOutSine);
	
		public static EaseTransition InExpo = new EaseTransition(_inExpo);
		public static EaseTransition OutExpo = new EaseTransition(_outExpo);
		public static EaseTransition InOutExpo = new EaseTransition(_inOutExpo);
	
		public static EaseTransition InCirc = new EaseTransition(_inCirc);
		public static EaseTransition OutCirc = new EaseTransition(_outCirc);
		public static EaseTransition InOutCirc = new EaseTransition(_inOutCirc);
	
		// TODO: implement
		public static EaseTransition InElastic = new EaseTransition(_inExpo);
		public static EaseTransition OutElastic = new EaseTransition(_outExpo);
		public static EaseTransition InOutElastic = new EaseTransition(_inOutExpo);
	
		public static EaseTransition InBack = new EaseTransition(_inExpo);
		public static EaseTransition OutBack = new EaseTransition(_outExpo);
		public static EaseTransition InOutBack = new EaseTransition(_inOutExpo);
	
		public static EaseTransition InBounce = new EaseTransition(_inExpo);
		public static EaseTransition OutBounce = new EaseTransition(_outExpo);
		public static EaseTransition InOutBounce = new EaseTransition(_inOutExpo);
	
		static float _linear(float start, float distance, float elapsedTime, float duration)
		{
			if (elapsedTime > duration) elapsedTime = duration;
	  		return distance * (elapsedTime / duration) + start;
		}
	
		static float _inQuad(float start, float distance, float elapsedTime, float duration)
		{
			elapsedTime = (elapsedTime > duration) ? 1.0f : elapsedTime / duration;
	  		return distance * elapsedTime * elapsedTime + start;
		}
	
		static float _outQuad(float start, float distance, float elapsedTime, float duration)
		{
			elapsedTime = (elapsedTime > duration) ? 1.0f : elapsedTime / duration;
			return -distance * elapsedTime * (elapsedTime - 2.0f) + start;
		}
	
		static float _inOutQuad(float start, float distance, float elapsedTime, float duration)
		{
			elapsedTime = (elapsedTime > duration) ? 2.0f : elapsedTime / (duration / 2);
			if (elapsedTime < 1) return distance / 2.0f * elapsedTime * elapsedTime + start;
			elapsedTime--;
			return -distance / 2.0f * (elapsedTime * (elapsedTime - 2.0f) - 1.0f) + start;
		}
	
		static float _inCubic(float start, float distance, float elapsedTime, float duration)
		{
			elapsedTime = (elapsedTime > duration) ? 1.0f : elapsedTime / duration;
			return distance * elapsedTime * elapsedTime * elapsedTime + start;
		}
	
		static float _outCubic(float start, float distance, float elapsedTime, float duration)
		{
			elapsedTime = (elapsedTime > duration) ? 1.0f : elapsedTime / duration;
			elapsedTime--;
			return distance * (elapsedTime * elapsedTime * elapsedTime + 1) + start;
		}
	 
		static float _inOutCubic(float start, float distance, float elapsedTime, float duration)
		{
			elapsedTime = (elapsedTime > duration) ? 2.0f : elapsedTime / (duration / 2);
			if (elapsedTime < 1)
				return distance / 2 * elapsedTime * elapsedTime * elapsedTime + start;
			elapsedTime -= 2;
			return distance / 2 * (elapsedTime * elapsedTime * elapsedTime + 2) + start;
		}
	 
		static float _inQuart(float start, float distance, float elapsedTime, float duration)
		{
			elapsedTime = (elapsedTime > duration) ? 1.0f : elapsedTime / duration;
			return distance * elapsedTime * elapsedTime * elapsedTime * elapsedTime + start;
		}
	
		static float _outQuart(float start, float distance, float elapsedTime, float duration)
		{
			elapsedTime = (elapsedTime > duration) ? 1.0f : elapsedTime / duration;
			elapsedTime--;
			return -distance * (elapsedTime * elapsedTime * elapsedTime * elapsedTime - 1) + start;
		}
	
		static float _inOutQuart(float start, float distance, float elapsedTime, float duration)
		{
			elapsedTime = (elapsedTime > duration) ? 2.0f : elapsedTime / (duration / 2);
			if (elapsedTime < 1)
				return distance / 2 * elapsedTime * elapsedTime * elapsedTime * elapsedTime + start;
			elapsedTime -= 2;
			return -distance / 2 * (elapsedTime * elapsedTime * elapsedTime * elapsedTime - 2) + start;
		}
	
		static float _inQuint(float start, float distance, float elapsedTime, float duration)
		{
			elapsedTime = (elapsedTime > duration) ? 1.0f : elapsedTime / duration;
			return distance * elapsedTime * elapsedTime * elapsedTime * elapsedTime * elapsedTime + start;
		}
	 
		static float _outQuint(float start, float distance, float elapsedTime, float duration)
		{
			elapsedTime = (elapsedTime > duration) ? 1.0f : elapsedTime / duration;
			elapsedTime--;
			return distance * (elapsedTime * elapsedTime * elapsedTime * elapsedTime * elapsedTime + 1) + start;
		}
	
		static float _inOutQuint(float start, float distance, float elapsedTime, float duration)
		{
			elapsedTime = (elapsedTime > duration) ? 2.0f : elapsedTime / (duration / 2);
			if (elapsedTime < 1)
				return distance / 2 * elapsedTime * elapsedTime * elapsedTime * elapsedTime * elapsedTime + start;
			elapsedTime -= 2;
			return distance / 2 * (elapsedTime * elapsedTime * elapsedTime * elapsedTime * elapsedTime + 2) + start;
		}
	
		static float _inSine(float start, float distance, float elapsedTime, float duration)
		{
			if (elapsedTime > duration)
				elapsedTime = duration;
			return -distance * Mathf.Cos(elapsedTime / duration * (Mathf.PI / 2)) + distance + start;
		}
	
		static float _outSine(float start, float distance, float elapsedTime, float duration)
		{
			if (elapsedTime > duration)
				elapsedTime = duration;
			return distance * Mathf.Sin(elapsedTime / duration * (Mathf.PI / 2)) + start;
		}
	
		static float _inOutSine(float start, float distance, float elapsedTime, float duration)
		{
			if (elapsedTime > duration)
				elapsedTime = duration;
			return -distance / 2 * (Mathf.Cos(Mathf.PI * elapsedTime / duration) - 1) + start;
		}
	
		static float _inExpo(float start, float distance, float elapsedTime, float duration)
		{
			if (elapsedTime > duration)
				elapsedTime = duration;
			return distance * Mathf.Pow(2, 10 * (elapsedTime / duration - 1)) + start;
		}
	 
		static float _outExpo(float start, float distance, float elapsedTime, float duration)
		{
			if (elapsedTime > duration)
				elapsedTime = duration;
			return distance * (-Mathf.Pow(2, -10 * elapsedTime / duration) + 1) + start;
		}
		 
		static float _inOutExpo(float start, float distance, float elapsedTime, float duration)
		{
			elapsedTime = (elapsedTime > duration) ? 2.0f : elapsedTime / (duration / 2);
			if (elapsedTime < 1)
				return distance / 2 * Mathf.Pow(2, 10 * (elapsedTime - 1)) + start;
			elapsedTime--;
			return distance / 2 * (-Mathf.Pow(2, -10 * elapsedTime) + 2) + start;
		}
	
		static float _inCirc(float start, float distance, float elapsedTime, float duration)
		{
			elapsedTime = (elapsedTime > duration) ? 1.0f : elapsedTime / duration;
			return -distance * (Mathf.Sqrt(1 - elapsedTime * elapsedTime) - 1) + start;
		}
	
		static float _outCirc(float start, float distance, float elapsedTime, float duration)
		{
			elapsedTime = (elapsedTime > duration) ? 1.0f : elapsedTime / duration;
			elapsedTime--;
			return distance * Mathf.Sqrt(1 - elapsedTime * elapsedTime) + start;
		}
	
		static float _inOutCirc(float start, float distance, float elapsedTime, float duration)
		{
			elapsedTime = (elapsedTime > duration) ? 2.0f : elapsedTime / (duration / 2);
			if (elapsedTime < 1)
				return -distance / 2 * (Mathf.Sqrt(1 - elapsedTime * elapsedTime) - 1) + start;
			elapsedTime -= 2;
			return distance / 2 * (Mathf.Sqrt(1 - elapsedTime * elapsedTime) + 1) + start;
		}
	
		/*static float _inElastic(float start, float distance, float elapsedTime, float duration)
		{
			if (elapsedTime > duration)
				elapsedTime = duration;
	
			if (elapsedTime == 0.0f)
				return start;
	
			if ((elapsedTime /= duration) == 1.0f)
			        return start + distance;
	
	        float p = duration * 0.3f;
	        float a = distance;
	        float s = p / 4.0f;
	        float postFix = a * Mathf.Pow(2.0f, 10.0 * (elapsedTime -= 1.0f));
	
	        return -(postFix * Mathf.Sin((elapsedTime * duration - s) * (2.0 * Mathf.PI) / p)) + start;
		}
	
		static float _outElastic(float start, float distance, float elapsedTime, float duration)
		{
			if (elapsedTime > duration)
				elapsedTime = duration;
	
			if (elapsedTime == 0.0f)
				return start;
	
			if ((elapsedTime /= duration) == 1.0f)
			        return start + distance;
	
	        float p = duration * 0.3f;
	        float a = distance;
	        float s = p / 4.0f;
	
	        return a * Mathf.Pow(2.0, -10.0 * elapsedTime) * Mathf.Sin((elapsedTime * duration - s) * (2.0 * Mathf.PI) / p) + distance + start;
		}
	
		static float _inOutElastic(float start, float distance, float elapsedTime, float duration)
		{
			// TODO: implement
			return 0.0f;
		}*/
		
	}
	
}