﻿using UnityEngine;
using System.Collections;

public class TweenerGlobal : MonoBehaviour {
	
	public static LaikaBOSS.Tweener instance;
	
	public LaikaBOSS.Tweener tweener;
	
	public void Start() {
		
		TweenerGlobal.instance = this.tweener;
		
	}
	
}
