﻿using LaikaBOSS;
using UnityEngine;

public class WindowAnimation : MonoBehaviourExt
{
    public virtual void OnInit(Transform componentsStorage)
    {
    }

    public virtual void OnReset(Window window, Tweener tweener, bool forward)
    {
    }

    public virtual void OnStart(Window window, Tweener tweener)
    {
    }

    public virtual void OnEnd(Window window, Tweener tweener)
    {
    }

    public virtual void OnShow(Window window, float time, Tweener tweener)
    {
    }

    public virtual void OnHide(Window window, float time, Tweener tweener)
    {
    }

    public virtual bool IsPostEffect()
    {
        return false;
    }

    public virtual bool IsSupported()
    {
        return true;
    }
}