﻿using System;
using System.Collections.Generic;
using LaikaBOSS;
using UnityEngine;
using Object = UnityEngine.Object;

namespace LaikaBOSS {
    [ExecuteInEditMode]
    public class Windows : MonoBehaviourExt {
#if UNITY_EDITOR
        public Window[] types;
        public string typesStoragePrefix;
#endif
        public WindowAnimation[] windowAnimations;
        public string[] typesStrings;

        public Tweener tweener;

        private float currentDepth = 20f;
        private float depthStep = .5f;

        private float zPosition = 1000f;
        private float zPositionStep = 100f;

        public static Windows instance;
        private static LinkedList<Window> windowsToWait = new LinkedList<Window>();
        private static readonly List<Window> windows = new List<Window>();

#if UNITY_EDITOR
        public void OnEnable() {
            typesStrings = new string[types.Length];
            for (int i = 0, count = types.Length; i < count; ++i) if (types[i] != null) typesStrings[i] = typesStoragePrefix + "Windows/" + types[i].gameObject.name;
        }
#endif

        public void Awake() {
            if (instance != null) return;
            instance = this;

            if (Application.isPlaying) foreach (var animation in windowAnimations) if (animation.IsSupported()) animation.OnInit(transform);
        }

        private static float GetNextDepth() {
            instance.currentDepth += instance.depthStep;
            return instance.currentDepth;
        }

        private static Vector3 GetNextPosition() {
            instance.zPosition += instance.zPositionStep;
            return new Vector3(0f, 0f, instance.zPosition);
        }

        public static int GetCountCreatedWithPostEffect() {
            return 0;
        }

        public static int GetCountCreated() {
            return windows.Count;
        }

        public static Window GetWindow(int index) {
            if (index < 0 || windows.Count <= index) return null;

            return windows[index];
        }

        public static Window GetWindowWithPostEffect(int index, Window exclude = null) {
            return null;
        }

        public static Window GetLastWindow() {
            var count = GetCountCreated();
            return count == 0 ? null : windows[count - 1];
        }

        public static float GetDepthStep() {
            return instance.depthStep;
        }

        public static void OnWindowHideStart(Window window) {
            windows.Remove(window);
        }

        public static void OnWindowDestroy(MonoBehaviourExt obj) {
            var win = obj as Window;
            if (win == null) return;
            instance.currentDepth -= instance.depthStep;
            win.OnDestroyEvent -= OnWindowDestroy;            

            var count = windows.Count;
            if (count == 0) OnAllWindowsClosed();
            else {
                var lastWin = windows[count - 1];

                lastWin.TurnOnUIEvents();
            }
        }

        public static void OnWindowCreate(Window window, object fromObject) {
            if (window.settings.depthIsStatic == false) window.camera.depth = GetNextDepth();

            window.transform.position = GetNextPosition();

            var count = windows.Count;
            Window lastWin = null;
            if (count > 1) lastWin = windows[count - 2];

            if (lastWin != null) lastWin.TurnOffUIEvents();            
        }

        public static void OnAllWindowsClosed() {
            ShowNext();
        }

        public static Window Show(WindowType windowType, bool force = false) {
            var window = CreateInstance(windowType);
            if (GetCountCreated() <= 0 || force) ShowNext();
            else window.gameObject.SetActive(false);

            return window;
        }

        private static void ShowNext() {
            if (windowsToWait.Count <= 0) return;
            var window = windowsToWait.First.Value;
            windowsToWait.RemoveFirst();
                
            if (!window.gameObject.activeSelf)
                window.gameObject.SetActive(true);
            windows.Add(window);

            GameAnalyticsEvents.ReportWindowOpen(window.GetWindowType());
        }

        private static Window Instantiate_INTERNAL(WindowType windowType) {
            return Instantiate(Resources.Load<Window>(instance.typesStrings[(int) windowType]));
        }

        private static Window CreateInstance(WindowType windowType) {
            var window = Instantiate_INTERNAL(windowType);
            window.OnDestroyEvent += OnWindowDestroy;

            OnWindowCreate(window, null);
            window.Init(windowType, null);
            DontDestroyOnLoad(window);
            windowsToWait.AddLast(window);

            return window;
        }
    }
}