﻿using System;
using LaikaBOSS;
using UnityEngine;

public class TwoButtonWindow : MonoBehaviour {
    public UILabel caption;
    public UILabel text;
    public UILabel okButton;
    public UILabel cancelButton;
    public UITexture avaTexture;
    public Transform panel;
    public Vector3 position;
    private Action okButtonAction;
    private Action cancelButtonAction;

    public static Window Show(string caption, string text, Action okAction, Texture texture = null, bool force = false) {
        return Show(caption, text, Localization.Get("OkKey"), Localization.Get("CancelKey"), okAction, texture: texture,
            force: force);
    }

    public static Window Show(string caption, string text, string okButton, string cancelButton, Action okAction = null,
        Action cancelAction = null, Texture texture = null, bool force = false) {
        var window = Windows.Show(WindowType.OkCancelWindow, force);
        var twoButtonWindow = window.GetComponent<TwoButtonWindow>();
        twoButtonWindow.okButtonAction = okAction;
        twoButtonWindow.cancelButtonAction = cancelAction;
        if (caption != null) twoButtonWindow.caption.text = caption;
        twoButtonWindow.text.text = text;
        twoButtonWindow.okButton.text = okButton;
        twoButtonWindow.cancelButton.text = cancelButton;

        if (texture != null) {
            twoButtonWindow.avaTexture.mainTexture = texture;
            twoButtonWindow.panel.localPosition = twoButtonWindow.position;
        }

        return window;
    }

    public void OkButtonClicked() {
        okButtonAction.CallCallback();
    }

    public void CancelButtonClicked() {
        cancelButtonAction.CallCallback();
    }
}