﻿using System;
using LaikaBOSS;
using UnityEngine;

public class OneButtonWindow : MonoBehaviour {
    public UILabel caption;
    public UILabel text;
    public UILabel okButton;
    public UITexture avaTexture;

    private Action okButtonAction;


    public static Window Show(string caption, string text, Texture texture = null, bool force = false) {
        return Show(caption, text, Localization.Get("OkKey"), force: force);
    }

    public static Window Show(string caption, string text, string okButton, Action okAction = null,
        Texture texture = null, bool force = false) {
        var window = Windows.Show(WindowType.OkWindow, force);
        var oneButton = window.GetComponent<OneButtonWindow>();
        oneButton.okButtonAction = okAction;

        oneButton.caption.text = caption;
        oneButton.text.text = text;
        oneButton.okButton.text = okButton;

        if (texture != null) oneButton.avaTexture.mainTexture = texture;

        return window;
    }

    public void OkButtonClicked() {
        okButtonAction.CallCallback();
    }
}