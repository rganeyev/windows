﻿using LaikaBOSS;
using UnityEngine;

public class WindowAnimationBlur : WindowAnimation
{
    #region SETTINGS

    public GameObject sourceFirst;
    public GameObject sourceSecond;

    public float strength = 3f;

    private Blur blurComponentFirst;
    private Blur blurComponentSecond;

    #endregion

    public override bool IsSupported()
    {
#if UNITY_EDITOR
        //return false;
#endif

#if UNITY_IPHONE
        if (iPhone.generation == iPhoneGeneration.iPad2Gen ||
            iPhone.generation == iPhoneGeneration.iPad3Gen ||
            iPhone.generation == iPhoneGeneration.iPhone4 ||
            iPhone.generation == iPhoneGeneration.iPhone4S)
        {
            return false;
        }
#endif

        return true;
    }

    public override bool IsPostEffect()
    {
        return true;
    }

    public override void OnInit(Transform componentsStorage)
    {
        GameObject inst;

        inst = Object.Instantiate(sourceFirst) as GameObject;
        inst.transform.parent = componentsStorage;
        inst.transform.localPosition = Vector3.zero;
        inst.transform.localRotation = Quaternion.identity;
        inst.transform.localScale = Vector3.zero;

        blurComponentFirst = inst.GetComponent<Blur>();

        inst = Object.Instantiate(sourceSecond) as GameObject;
        inst.transform.parent = componentsStorage;
        inst.transform.localPosition = Vector3.zero;
        inst.transform.localRotation = Quaternion.identity;
        inst.transform.localScale = Vector3.zero;

        blurComponentSecond = inst.GetComponent<Blur>();
    }

    public override void OnStart(Window window, Tweener tweener)
    {
        tweener.removeTweens(window);
    }

    public override void OnEnd(Window window, Tweener tweener)
    {
    }

    public override void OnReset(Window window, Tweener tweener, bool forward)
    {
        Reset(window, tweener, forward);
    }

    public override void OnShow(Window window, float time, Tweener _tweener)
    {
        SetBlur(window, time, Windows.instance.tweener);
    }

    public override void OnHide(Window window, float time, Tweener _tweener)
    {
        UnsetBlur(window, time, Windows.instance.tweener);
    }

    #region EFFECT

    public void Reset(Window window, Tweener tweener, bool forward)
    {
    }

    private void SetEffect(Blur comp, float depth, float time, Tweener tweener, float from, float to)
    {
        // Переставляем эффект под предпоследнюю камеру
        comp.camera.depth = depth;

        comp.enabled = true;
        // Включаем твинер на текущий эффект
        tweener.removeTweens(comp);
        tweener.addTween(comp, time, from, to).onUpdate((Blur blur, float value) =>
        {
            blur.enabled = true;
            blur.blurSize = Mathf.Lerp(0f, strength, value);
        })
            .onComplete((Blur blur) => { if (to <= 0f) comp.enabled = false; })
            .onCancel((Blur blur) => { if (to <= 0f) comp.enabled = false; })
            .tag(comp);
    }

    public void SetBlur(Window window, float time, Tweener tweener)
    {
        // Рисуем эффект под текущим окном
        SetEffect(blurComponentFirst, window.camera.depth - Windows.GetDepthStep()/2f, time, tweener, 0f, 1f);

        // Если есть окно ниже текущего - рисуем эффект под ним
        int count = Windows.GetCountCreatedWithPostEffect();
        if (count > 0)
        {
            var win = Windows.GetWindowWithPostEffect(count - 1, window);
            if (win != null)
            {
                SetEffect(blurComponentSecond, win.camera.depth - Windows.GetDepthStep()/2f, time, tweener, 1f, 0f);
            }
            else
            {
                blurComponentSecond.blurSize = 0f;
                blurComponentSecond.enabled = false;
            }
        }
        else
        {
            blurComponentSecond.blurSize = 0f;
            blurComponentSecond.enabled = false;
        }
    }

    public void UnsetBlur(Window window, float time, Tweener tweener)
    {
        // Рисуем эффект под текущим окном
        SetEffect(blurComponentFirst, window.camera.depth - Windows.GetDepthStep()/2f, time, tweener, 1f, 0f);

        // Если есть окно ниже текущего - рисуем эффект под ним
        int count = Windows.GetCountCreatedWithPostEffect();
        if (count > 0)
        {
            var win = Windows.GetWindowWithPostEffect(count - 1, window);
            if (win != null)
            {
                SetEffect(blurComponentSecond, win.camera.depth - Windows.GetDepthStep()/2f, time, tweener, 0f, 1f);
            }
            else
            {
                blurComponentSecond.blurSize = 0f;
                blurComponentSecond.enabled = false;
            }
        }
        else
        {
            blurComponentSecond.blurSize = 0f;
            blurComponentSecond.enabled = false;
        }
    }

    #endregion
}