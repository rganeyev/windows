﻿using UnityEngine;
using LaikaBOSS;
using System.Collections;

public class WindowAnimationCameraRect : WindowAnimation {
	
	public override void OnStart(Window window, Tweener tweener) {
		
		tweener.removeTweens(window);
		
	}
	
	public override void OnEnd(Window window, Tweener tweener) {
	}
	
	public override void OnShow(Window window, float time, Tweener tweener) {

		tweener.addTween(window, time, 0f, 1f).ease(LaikaBOSS.Ease.OutExpo).onUpdate((Window _window, float value) => {

			if (_window != null) this.SetCameraRect(_window.camera, false, value);
			
		}).tag(window);
		
	}
	
	public override void OnHide(Window window, float time, Tweener tweener) {

		tweener.addTween(window, time, 1f, 0f).ease(LaikaBOSS.Ease.OutExpo).onUpdate((Window _window, float value) => {

			if (_window != null) this.SetCameraRect(_window.camera, true, value);
			
		}).tag(window);
		
	}

	Rect normalRect = new Rect(0f, 0f, 1f, 1f);
	Rect endToRect = new Rect(0.1f, 0.1f, 0.8f, 0.8f);
	float endFromSize = 0.75f;
	
	private void SetCameraRect(Camera camera, bool useRect, float value) {
		
		if (useRect == true) {
			
			camera.rect = new Rect(
				Mathf.Lerp(this.endToRect.x, this.normalRect.x, value),
				Mathf.Lerp(this.endToRect.y, this.normalRect.y, value),
				Mathf.Lerp(this.endToRect.width, this.normalRect.width, value),
				Mathf.Lerp(this.endToRect.height, this.normalRect.height, value)
				);
			
		} else {
			
			camera.rect = this.normalRect;
			camera.orthographicSize = Mathf.Lerp(this.endFromSize, 1f, value);
			
		}
		
	}

}
