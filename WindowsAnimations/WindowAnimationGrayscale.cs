﻿using LaikaBOSS;
using UnityEngine;
using System.Collections;

public class WindowAnimationGrayscale : WindowAnimation {
	
	#region SETTINGS
	public GameObject sourceFirst;
	public GameObject sourceSecond;

	public float strength = 1f;
	
	private GrayscaleEffect grayscaleComponentFirst;
	private GrayscaleEffect grayscaleComponentSecond;
	#endregion
	
	public override bool IsPostEffect() {
		
		return true;
		
	}

	public override void OnInit(Transform componentsStorage) {
		
		GameObject inst;
		
		inst = GameObject.Instantiate(this.sourceFirst) as GameObject;
		inst.transform.parent = componentsStorage;
		inst.transform.localPosition = Vector3.zero;
		inst.transform.localRotation = Quaternion.identity;
		inst.transform.localScale = Vector3.zero;
		
		this.grayscaleComponentFirst = inst.GetComponent<GrayscaleEffect>();
		
		inst = GameObject.Instantiate(this.sourceSecond) as GameObject;
		inst.transform.parent = componentsStorage;
		inst.transform.localPosition = Vector3.zero;
		inst.transform.localRotation = Quaternion.identity;
		inst.transform.localScale = Vector3.zero;
		
		this.grayscaleComponentSecond = inst.GetComponent<GrayscaleEffect>();
		
	}
	
	public override void OnStart(Window window, LaikaBOSS.Tweener tweener) {
		
		tweener.removeTweens(this.grayscaleComponentFirst);
		tweener.removeTweens(this.grayscaleComponentSecond);
		
	}
	
	public override void OnEnd(Window window, LaikaBOSS.Tweener tweener) {
	}

	public override void OnReset(Window window, LaikaBOSS.Tweener tweener, bool forward) {
		
		this.Reset(window, tweener, forward);

	}

	public override void OnShow(Window window, float time, LaikaBOSS.Tweener tweener) {
		
		this.SetGrayscale(window, time, tweener);
		
	}
	
	public override void OnHide(Window window, float time, LaikaBOSS.Tweener tweener) {
		
		this.UnsetGrayscale(window, time, tweener);
		
	}
	
	#region EFFECT
	private void SetEffect(GrayscaleEffect comp, float depth, float time, LaikaBOSS.Tweener tweener, float from, float to) {
		
		// Переставляем эффект под предпоследнюю камеру
		comp.camera.depth = depth;
		
		comp.enabled = true;
		// Включаем твинер на текущий эффект
		tweener.removeTweens(comp);
		tweener.addTween(comp, time, from, to).onUpdate((GrayscaleEffect eff, float value) => {
			
			comp.enabled = true;
			comp.strength = Mathf.Lerp(0f, this.strength, value);
			
		}).onComplete((GrayscaleEffect eff) => {
			
			if (to <= 0f) comp.enabled = false;
			
		}).onCancel((GrayscaleEffect eff) => {
			
			if (to <= 0f) comp.enabled = false;

		}).tag(comp);
		
	}

	public void Reset(Window window, LaikaBOSS.Tweener tweener, bool forward) {

	}

	public void SetGrayscale(Window window, float time, LaikaBOSS.Tweener tweener) {
		
		// Рисуем эффект под текущим окном
		this.SetEffect(this.grayscaleComponentFirst, window.camera.depth - Windows.GetDepthStep() / 2f, time, tweener, 0f, 1f);
		
		// Если есть окно ниже текущего - рисуем эффект под ним
		int count = Windows.GetCountCreatedWithPostEffect();
		if (count > 0) {
			
			var win = Windows.GetWindowWithPostEffect(count - 1, window);
			if (win != null) {
				
				this.SetEffect(this.grayscaleComponentSecond, win.camera.depth - Windows.GetDepthStep() / 2f, time, tweener, 1f, 0f);
				
			} else {
				
				this.grayscaleComponentSecond.strength = 0f;
				this.grayscaleComponentSecond.enabled = false;
				
			}
			
		} else {
			
			this.grayscaleComponentSecond.strength = 0f;
			this.grayscaleComponentSecond.enabled = false;
			
		}

	}
	
	public void UnsetGrayscale(Window window, float time, LaikaBOSS.Tweener tweener) {
		
		// Рисуем эффект под текущим окном
		this.SetEffect(this.grayscaleComponentFirst, window.camera.depth - Windows.GetDepthStep() / 2f, time, tweener, 1f, 0f);
		
		// Если есть окно ниже текущего - рисуем эффект под ним
		int count = Windows.GetCountCreatedWithPostEffect();
		if (count > 0) {
			
			var win = Windows.GetWindowWithPostEffect(count - 1, window);
			if (win != null) {
				
				this.SetEffect(this.grayscaleComponentSecond, win.camera.depth - Windows.GetDepthStep() / 2f, time, tweener, 0f, 1f);
				
			} else {
				
				this.grayscaleComponentSecond.strength = 0f;
				this.grayscaleComponentSecond.enabled = false;
				
			}
			
		} else {
			
			this.grayscaleComponentSecond.strength = 0f;
			this.grayscaleComponentSecond.enabled = false;
			
		}

	}
	#endregion

}
