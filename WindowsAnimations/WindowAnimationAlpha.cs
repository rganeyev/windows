﻿using LaikaBOSS;

public class WindowAnimationAlpha : WindowAnimation
{
    public override void OnStart(Window window, Tweener tweener)
    {
        tweener.removeTweens(window);
    }

    public override void OnEnd(Window window, Tweener tweener)
    {
    }

    public override void OnShow(Window window, float time, Tweener tweener)
    {
        tweener.addTween(window, time, 0f, 1f)
            .ease(LaikaBOSS.Ease.OutExpo)
            .onUpdate((_window, value) => { if (_window != null) _window.SetAlpha(value); })
            .tag(window);
    }

    public override void OnHide(Window window, float time, Tweener tweener)
    {
        tweener.addTween(window, time, 1f, 0f)
			.ease(LaikaBOSS.Ease.OutExpo)
            .onUpdate((_window, value) => { if (_window != null) _window.SetAlpha(value); })
            .tag(window);
    }
}