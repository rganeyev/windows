﻿using System;
using System.Collections;
using System.Collections.Generic;
using LaikaBOSS;
using UnityEngine;

[ExecuteInEditMode]
public class Window : MonoBehaviourExt
{
    
	public event Action<Window> OnShowStart;
    public event Action<Window> OnHideStart;
    public event Action<Window> OnShowEnd;
    public event Action<Window> OnHideEnd;
	    
    public event Action<Window, object> OnManualEvent;

    public string title = string.Empty;

    public UICamera uiCamera;

    private float showTime;
    private float hideTime;

    private WindowType windowType;
    [NonSerialized] public object fromObjectSystem;
    private bool inited;

    public Settings settings;

    public UIPanel[] panels;
    public UIPanel[] panelsInner;
    public Transform root;

    public class ManualEvent
    {
        private readonly Window window;
        private readonly Action<Window> action;

        public ManualEvent(Window window, Action<Window> action)
        {
            this.window = window;
            this.action = action;
        }

        public void Raise()
        {
            if (action != null) action(window);
        }
    }

#if UNITY_EDITOR
    public void OnEnable()
    {
        if (root == null)
            root = transform.Find("Root");

        panels = GetComponentsInChildren<UIPanel>(true);
        var pList = new List<UIPanel>();
        var pListInner = new List<UIPanel>();
        foreach (var panel in panels)
        {
            var parent = NGUITools.FindInParents<UIPanel>(panel.gameObject);
            if (parent != null)
            {
                pListInner.Add(panel);
            }
            else
            {
                pList.Add(panel);
            }
        }

        panels = pList.ToArray();
        panelsInner = pListInner.ToArray();
    }
#endif

    public string GetTitle()
    {
        return title;
    }

    public WindowType GetWindowType()
    {
        return windowType;
    }

    public void TurnOffUIEvents()
    {
        if (uiCamera != null) uiCamera.enabled = false;
    }

    public void TurnOnUIEvents()
    {
        if (uiCamera != null) uiCamera.enabled = true;
    }
	
    [Serializable]
    public class Settings
    {
        public enum Type
        {
            Manual,
            Timed,
        }

        public Type type;
        public float time;

        public bool depthIsStatic = false;
        public bool registerCameraEvents = true;

        public bool hideOnNullEvent = false;

        public bool animate = true;

        public WindowAnimation[] windowAnimations;

        public Tweener animationTweener = null;

        public float showSpeed = 1f;
        public float hideSpeed = 1f;        

        public void ApplyOnShowStart(Window window)
        {
            StartAnimation(window, true);
        }

        public void ApplyOnShowEnd(Window window)
        {
            EndAnimation(window, true);
        }

        public void ApplyOnHideStart(Window window)
        {
            StartAnimation(window, false);
        }

        public void ApplyOnHideEnd(Window window)
        {
            EndAnimation(window, false);

            Destroy(window.gameObject);
        }

        private void StartAnimation(Window window, bool forward)
        {
            if (animationTweener == null) return;

            if (animate && forward)
            {
                foreach (var animation in windowAnimations)
                    if (animation.IsSupported()) animation.OnStart(window, animationTweener);
                foreach (var animation in windowAnimations)
                    if (animation.IsSupported()) animation.OnShow(window, showSpeed, animationTweener);
                foreach (var animation in windowAnimations)
                    if (animation.IsSupported()) animation.OnEnd(window, animationTweener);
            }

            if (animate && forward == false)
            {
                foreach (var animation in windowAnimations)
                    if (animation.IsSupported()) animation.OnStart(window, animationTweener);
                foreach (var animation in windowAnimations)
                    if (animation.IsSupported()) animation.OnHide(window, hideSpeed, animationTweener);
                foreach (var animation in windowAnimations)
                    if (animation.IsSupported()) animation.OnEnd(window, animationTweener);
            }
        }

        private void EndAnimation(Window window, bool forward)
        {
            if (animationTweener == null) return;

            if (animate && forward)
            {
                foreach (var animation in windowAnimations)
                    if (animation.IsSupported()) animation.OnReset(window, animationTweener, forward);
            }

            if (animate && forward == false)
            {
                foreach (var animation in windowAnimations)
                    if (animation.IsSupported()) animation.OnReset(window, animationTweener, forward);
            }
        }
    }

    private void OnPitchHide(Vector2 fingerPos1, Vector2 fingerPos2)
    {
        Hide();
    }

    public void SetAlpha(float value)
    {
        foreach (var panel in panels)
        {
            panel.alpha = value;
        }

        foreach (var panel in panelsInner)
        {
            panel.alpha = value;
        }
    }

    public void Init(WindowType windowType, object fromObject)
    {
        if (inited) return;

        this.windowType = windowType;
        fromObjectSystem = fromObject;

        InitOther();

        inited = true;
    }

   private void InitOther()
    {
        // init tweeners
        showTime = settings.animate ? settings.showSpeed : 0f;
        hideTime = settings.animate ? settings.hideSpeed : 0f;
    }

    public void Show()
    {
        Init(windowType, fromObjectSystem);

        StartCoroutine(ShowInternal());
    }

    private IEnumerator ShowInternal()
    {
        if (OnShowStart != null) OnShowStart(this);
        settings.ApplyOnShowStart(this);

        //yield return this.StartCoroutine(this.WaitForSecondsFixed(this.showTime));
        yield return new WaitForSeconds(showTime);

        settings.ApplyOnShowEnd(this);

        if (OnShowEnd != null) OnShowEnd(this);

        if (settings.type == Settings.Type.Timed)
        {
            Hide(settings.time);
        }
    }

    public void Hide(float delay)
    {
        StartCoroutine(HideInternal(delay));
    }

    public void Hide()
    {
        StartCoroutine(HideInternal());
    }

    private IEnumerator HideInternal(float delay = 0f)
    {
        Windows.OnWindowHideStart(this);

        if (delay < 0f) delay = 0f;
        if (delay > 0f) yield return StartCoroutine(WaitForSecondsFixed(delay));

        settings.ApplyOnHideStart(this);

        if (OnHideStart != null) OnHideStart(this);
        yield return new WaitForSeconds(hideTime);

        if (OnHideEnd != null) OnHideEnd(this);

        settings.ApplyOnHideEnd(this);        
    }

    public void SendEvent(object obj)
    {
        if (OnManualEvent != null)
            OnManualEvent(this, obj);
    }
}